#!/usr/bin/env python
import os

from automan.api import PySPHProblem
from automan.api import Automator, Simulation, filter_cases
import numpy as np
import matplotlib
matplotlib.use('pdf')

BASE_HDX = 1.2
HDX = 1.2
# RESOLUTIONS = [50, 100, 200, 250, 400, 500, 1000]
# RESOLUTIONS = [50, 100, 200, 250, 400, 500]
# RESOLUTIONS = [50, 100, 200, 250, 400]
RESOLUTIONS = [50, 100, 200, 250, 400, 500]
# RESOLUTIONS = [50, 100]
pfreq = 20
CLEAN = True
NCORE, NTHREAD = 1, 10
# NCORE, NTHREAD = 2, 4
STEPS = 100
# INTG = " --intg euler "
INTG = ""
# ERRORS = ['p_linf', 'linf']
ERRORS = ['p_l1', 'l1']

COLORS_SOLID = {
   'MMS':'black',
   'MMS2Layer':'blue',
   'Colagrossi':'green',
   'Marrone':'red',
   'Adami':'cyan',
   'Marongiu':'maroon',
   'Hashemi':'orange',
   'RkpmMarrone':'lime',
   'New':'indigo',
   'Randles':'chocolate',
   'Takeda':'steelblue',
   'FourtakasNew':'magenta',
   'Esmaili':'brown',

   'Donothing':'green',
    'Mirror':'red',
    'Hybrid':'cyan',
    'MirrorNew':'maroon',
    'Exact':'black'
}

def get_convergence_data(cases, nx='nx', error='l1'):
    data = {}
    for case in cases:
        l1 = case.data[error]
        l1 = sum(l1)/len(l1)
        data[case.params[nx]] = l1
    nx_arr = np.asarray(sorted(data.keys()), dtype=float)
    l1 = np.asarray([data[x] for x in nx_arr])
    return nx_arr, l1


def get_convergence_data_raw(cases, nx='nx', error='l1'):
    data = {}
    for case in cases:
        l1 = case.data[error]
        data[case.params[nx]] = l1
    return data


def get_cpu_time(case):
    import glob
    import json
    info = glob.glob(case.input_path('*.info'))[0]
    with open(info) as fp:
        data = json.load(fp)
    return round(data['cpu_time'], 2)


def make_table(column_names, row_data, output_fname, sort_cols=None,
               multicol=None, **extra_kw):
    import pandas as pd
    col_data = [[] for i in range(len(column_names))]
    for row in row_data:
        for i, item in enumerate(row):
            col_data[i].append(item)
    d = {n: col_data[i] for i, n in enumerate(column_names)}
    df = pd.DataFrame(d, columns=column_names)
    if sort_cols:
        sort_by = [column_names[i] for i in sort_cols]
    else:
        sort_by = column_names[-1]
    df.sort_values(by=sort_by, inplace=True)
    parent = os.path.dirname(output_fname)
    if not os.path.exists(parent):
        os.mkdir(parent)
    with open(output_fname, 'w') as fp:
        fp.write(df.to_latex(index=False, escape=False, **extra_kw))
        fp.close()
    if multicol is not None:
        import fileinput
        for line in fileinput.FileInput(output_fname, inplace=1):
            if "\\toprule" in line:
                line = line.replace(line, multicol)
            print(line, end='')


class MMSConv(PySPHProblem):
    def __init__(
        self, sim, out, tf=2.0, common_cmd='', name='', cores=0, threads=0,
        err=['l1', 'p_l1'], res=RESOLUTIONS, filename='mms_main.py'):
        self.tf = tf
        self.cmd = common_cmd
        self.name = name
        self.cores = cores
        self.threads = threads
        self.errs = err
        self.res = res
        self.filename = filename

        super(MMSConv, self).__init__(sim, out)

    def get_file(self):
        return 'python ' + os.path.join('code', self.filename)

    def _make_cases(self, cmd, tf_override=None, hdx_override=0.0):
        get_path = self.input_path
        self.nx = self.res
        self.re = [100]
        self.kernel = 'QuinticSpline'
        self.hdx = HDX
        if hdx_override > 0.1:
            self.hdx = hdx_override

        tf = self.tf
        if tf_override is not None:
            tf = tf_override

        n_core = self.cores
        n_thread = self.threads

        cmd_split = cmd.split()
        cmd_extra = self.cmd.split()

        # remove commands already in the base class
        ##########
        to_remove = []
        for i in range(int(len(cmd_extra)/2)):
            if cmd_extra[2*i] in cmd_split:
                to_remove.append(cmd_extra[2*i])
                to_remove.append(cmd_extra[2*i+1])
        for _cmd in to_remove:
            cmd_extra.remove(_cmd)

        extra = ' '
        for _cmd in cmd_extra:
            extra += _cmd
            extra += ' '
        ##########

        _cmd = self.get_file() + ' --openmp ' + cmd + extra
        cases = [
            Simulation(
                get_path('re_%s_nx_%d' % (re, nx)), _cmd,
                job_info=dict(
                    n_core=n_core, n_thread=n_thread), nx=nx, re=re, tf=tf,
                    kernel=self.kernel, pfreq=pfreq, hdx=self.hdx
            )
            for re in self.re for nx in self.nx
        ]

        return cases

    def run(self):
        self.make_output_dir()
        # self._plot_convergence_rate(error='l1')
        # self._plot_convergence_rate(error='p_l1')
        # self._plot_convergence_rate(error='linf')
        # self._plot_convergence_rate(error='p_linf')
        # self._plot_l1_with_time(error='linf')
        # self._plot_l1_with_time(error='p_linf')

    def _plot_convergence_rate(self, error='l1'):
        import matplotlib.pyplot as plt
        style = ['g-o', 'b-o', 'r-o', 'k-0']
        plt.figure(1)
        plt.figure(2)
        l1, dx = None, None
        for re, ls in zip(self.re, style):
            cases = filter_cases(self.cases, re=re)
            nx, l1 = get_convergence_data(cases, nx='nx', error=error)
            plt.figure(1)
            plt.loglog(1.0/nx, l1, ls, label='Re=%s' % re)
            hdx = self.hdx#get_new_hdx2(nx, BASE_HDX, 50)
            dx = 1.0/(nx)*hdx
            ldx = np.log(dx)
            ll1 = np.log(l1)
            slope = (ll1[1:] - ll1[:-1])/(ldx[1:] - ldx[:-1])
            plt.figure(2)
            plt.plot(dx[1:], slope, ls, label='Re=%s' % re)

        plt.figure(1)
        plt.loglog(dx, l1[0]*(dx/dx[0])**2, 'k--', linewidth=2,
                   label=r'$O(\Delta s^2)$')
        plt.loglog(dx, l1[0]*(dx/dx[0]), 'k:', linewidth=2,
                   label=r'$O(\Delta s)$')
        plt.grid()
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'$L_1$ error')
        plt.savefig(self.output_path('%s_conv.png')%error)
        plt.close(1)

        plt.figure(2)
        plt.grid()
        plt.legend(loc='best')
        plt.xlabel(r'$h$')
        plt.ylabel(r'Convergence order')
        plt.savefig(self.output_path('%s_conv_order.png')%error)
        plt.close(2)

    def _plot_l1_with_time(self, error):
        import matplotlib.pyplot as plt
        style = ['g-o', 'b-o', 'r-o', 'k-0']
        plt.figure()
        for re, ls in zip(self.re, style):
            cases = filter_cases(self.cases, re=re)
            data = get_convergence_data_raw(cases, nx='nx', error=error)
            print(error, data)
            for nx in self.nx:
                plt.plot(data[nx], label='nx=%d'%nx)

        plt.grid()
        plt.legend(loc='best')
        plt.xlabel(r'data')
        plt.ylabel(error)
        plt.savefig(self.output_path('%s_with_time.png')%error)
        plt.close()


class Comparison(PySPHProblem):
    def _make_case(self, klass, tf=2.0, common_cmd='', name='',
                   threads=NTHREAD, cores=NCORE, err=['p_l1', 'l1'],
                   res=RESOLUTIONS, filename='mms_main.py'):
        self.err = err
        self.problems = {
            cls.__name__: cls(
                self.sim_dir, self.out_dir, tf=tf, common_cmd=common_cmd,
                name=name, cores=cores, threads=threads, err=err, res=res, filename=filename)
            for cls in klass
        }

    def _set_re(self, name):
        self.re = self.problems[name].re
        self.nx = self.problems[name].nx
        self.hdx = self.problems[name].hdx

    def get_label(self, problem):
        return ''

    def get_color(self, problem):
        print(problem)
        return COLORS_SOLID[problem]

    def get_requires(self):
        return list(self.problems.items())

    def run(self):
        self.make_output_dir()
        self._plot_convergence_rate(err=self.err)
        self.remove_data()

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for problem in self.problems.values():
                cases = problem.cases
                for case in cases:
                    dirname = case.input_path()
                    path = os.path.join(dirname , 'mms*.hdf5')
                    names0 = glob.glob(path)
                    path = os.path.join(dirname , 'mms*.npz')
                    names1 = glob.glob(path)
                    names = names0 + names1
                    for name in names:
                        print("removing", name)
                        os.remove(name)

    def _local_plot_modify(self, ax):
        pass

    def _plot_convergence_rate(self, err):
        import matplotlib.pyplot as plt
        from matplotlib.ticker import MultipleLocator
        available = (
            list('bgrcmyk') +
            ['darkred', 'chocolate', 'darkorange', 'lime', 'darkgreen',
             'fuchsia', 'navy', 'indigo']
        )
        colors = {
            scheme: available[i]
            for i, scheme in enumerate(sorted(self.problems.keys()))
        }

        max_nx = 0
        fig, axes = plt.subplots(1, 2, sharey=True, figsize=(10, 5))
        errors = err
        for i, error in enumerate(errors):
            for re in self.re:
                dx = None
                index = 0
                for scheme, problem in self.problems.items():
                    cases = filter_cases(problem.cases, re=re)
                    nx, l1 = get_convergence_data(cases, nx='nx', error=error)
                    max_nx = max(nx.max(), max_nx)
                    label = self.get_label(problem)
                    hdx = problem.hdx
                    dx = 1.0/(nx)#*hdx
                    axes[i].loglog(dx, l1 #* np.exp(.2*index)
                                   ,linestyle='-', color=self.get_color(scheme),
                                   marker='o', label=label)
                    index += 1

                axes[i].loglog(dx, l1[0]*(dx/dx[0])**2, 'k--', linewidth=2,
                        label=r'$O(\Delta s^2)$')
                axes[i].loglog(dx, l1[0]*(dx/dx[0]), 'k:', linewidth=2,
                        label=r'$O(\Delta s)$')
                self._local_plot_modify(axes[i])
                axes[i].set_xlabel(r'$\Delta s$')
                if (error == 'p_l1') or (error == 'p_linf') or (error == 'p_l2'):
                    axes[i].text(0.85, 0.05, 'Pressure', transform=axes[i].transAxes)
                elif (error == 'l1') or (error == 'linf') or (error == 'l2'):
                    axes[i].text(0.85, 0.05, 'Velocity', transform=axes[i].transAxes)
                if i==0:
                    if errors[0] == 'p_l1':
                        axes[i].set_ylabel(r'$L_1$ error')
                    elif errors[0] == 'p_l2':
                        axes[i].set_ylabel(r'$L_2$ error')
                    else:
                        axes[i].set_ylabel(r'$L_\infty$ error')
                axes[i].grid()

        handles, labels = axes[0].get_legend_handles_labels()
        fig.legend(handles, labels, bbox_to_anchor=[0.5, 0.92], ncol=5, loc='center')
        plt.tight_layout()
        plt.subplots_adjust(top=0.83, hspace=0.0, wspace=0.0)
        plt.savefig(self.output_path(self.get_name() + '_%s_conv_re_%s.png' %(errors[1],re)))
        plt.close()


class MMS1(MMSConv):
    def get_name(self):
        return 'mms_1' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --nu 0.0 --mms mms1 --timestep 0.00004545 '
        )
        self.cases = self._make_cases(cmd)


class MMS2(MMSConv):
    def get_name(self):
        return 'mms_2' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --nu 1. --mms mms2 --timestep 0.0000005 '
        )
        self.cases = self._make_cases(cmd)


class MMS3(MMSConv):
    def get_name(self):
        return 'mms_3' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --nu 0.01 --mms mms3 --timestep 0.00004545 '
        )
        self.cases = self._make_cases(cmd)


class MMS4(MMSConv):
    def get_name(self):
        return 'mms_4' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --nu 0.0 --mms mms4 --timestep 0.00004545 '
        )
        self.cases = self._make_cases(cmd)


class MMS6(MMSConv):
    def get_name(self):
        return 'mms_6' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --nu 0.01 --mms mms6 --timestep 0.00004545 '
        )
        self.cases = self._make_cases(cmd)


class MMS7(MMSConv):
    def get_name(self):
        return 'mms_7' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --nu 0.0 --mms mms7 --timestep 0.00004545 '
        )
        self.cases = self._make_cases(cmd)


class MMS8(MMSConv):
    def get_name(self):
        return 'mms_8' + self.name

    def setup(self):
        cmd = (
            ' --scheme tsph --scm wcsph --eos linear --pst-freq 10 --nu 0.0 --mms mms8 --timestep 0.00004545 '
        )
        self.cases = self._make_cases(cmd)


class Pressure(MMSConv):
    def get_name(self):
        return 'pressure' + self.name

    def setup(self):
        cmd = (
            ' --bc p_solid '
        )
        self.cases = self._make_cases(cmd)


class NoSlip(MMSConv):
    def get_name(self):
        return 'no_slip' + self.name

    def setup(self):
        cmd = (
            ' --bc u_no_slip '
        )
        self.cases = self._make_cases(cmd)


class Slip(MMSConv):
    def get_name(self):
        return 'slip' + self.name

    def setup(self):
        cmd = (
            ' --bc u_slip '
        )
        self.cases = self._make_cases(cmd)


class MMS2Layer(MMSConv):
    def get_name(self):
        return 'mms_2_layer' + self.name

    def setup(self):
        cmd = (
            ' --bctype mms --nl 2 --pst-freq 0 '
        )
        self.cases = self._make_cases(cmd)


class MMS(MMSConv):
    def get_name(self):
        return 'mms' + self.name

    def setup(self):
        cmd = (
            ' --bctype mms '
        )
        self.cases = self._make_cases(cmd)


class Takeda(MMSConv):
    def get_name(self):
        return 'takeda' + self.name

    def setup(self):
        cmd = (
            ' --bctype takeda '
        )
        self.cases = self._make_cases(cmd)


class Adami(MMSConv):
    def get_name(self):
        return 'adami' + self.name

    def setup(self):
        cmd = (
            ' --bctype adami '
        )
        self.cases = self._make_cases(cmd)


class Marongiu(MMSConv):
    def get_name(self):
        return 'marongiu' + self.name

    def setup(self):
        cmd = (
            ' --bctype marongiu '
        )
        self.cases = self._make_cases(cmd)


class Hashemi(MMSConv):
    def get_name(self):
        return 'hashemi' + self.name

    def setup(self):
        cmd = (
            ' --bctype hashemi --pst-freq 0 '
        )
        self.cases = self._make_cases(cmd)


class Randles(MMSConv):
    def get_name(self):
        return 'randles' + self.name

    def setup(self):
        cmd = (
            ' --bctype randles'
        )
        self.cases = self._make_cases(cmd)


class Colagrossi(MMSConv):
    def get_name(self):
        return 'colagrossi' + self.name

    def setup(self):
        cmd = (
            ' --bctype colagrossi '
        )
        self.cases = self._make_cases(cmd)


class Marrone(MMSConv):
    def get_name(self):
        return 'marrone' + self.name

    def setup(self):
        cmd = (
            ' --bctype marrone '
        )
        self.cases = self._make_cases(cmd)


class RkpmMarrone(MMSConv):
    def get_name(self):
        return 'rkpm_marrone' + self.name

    def setup(self):
        cmd = (
            ' --bctype new_marrone '
        )
        self.cases = self._make_cases(cmd)



class Esmaili(MMSConv):
    def get_name(self):
        return 'esmaili' + self.name

    def setup(self):
        cmd = (
            ' --bctype esmaili '
        )
        self.cases = self._make_cases(cmd)


class FourtakasNew(MMSConv):
    def get_name(self):
        return 'fourtakas_new' + self.name

    def setup(self):
        cmd = (
            ' --bctype new_fourtakas '
        )
        self.cases = self._make_cases(cmd)


class New(MMSConv):
    def get_name(self):
        return 'new' + self.name

    def setup(self):
        cmd = (
            ' --bctype new '
        )
        self.cases = self._make_cases(cmd)


class Donothing(MMSConv):
    def get_name(self):
        return 'donothing' + self.name

    def setup(self):
        cmd = (
            ' --bctype donothing '
        )
        self.cases = self._make_cases(cmd)


class Mirror(MMSConv):
    def get_name(self):
        return 'mirror' + self.name

    def setup(self):
        cmd = (
            ' --bctype mirror '
        )
        self.cases = self._make_cases(cmd)


class MirrorNew(MMSConv):
    def get_name(self):
        return 'mirror_new' + self.name

    def setup(self):
        cmd = (
            ' --bctype mirror_new '
        )
        self.cases = self._make_cases(cmd)


class Hybrid(MMSConv):
    def get_name(self):
        return 'hybrid' + self.name

    def setup(self):
        cmd = (
            ' --bctype hybrid '
        )
        self.cases = self._make_cases(cmd)


comp1 = [MMS1, MMS3]

pressure_bc = [MMS, MMS2Layer, Colagrossi, Marrone, Adami, Marongiu, Hashemi, FourtakasNew]
pressure_bc_2 = [MMS, MMS2Layer, Colagrossi, Marrone, Adami, Marongiu, Hashemi, FourtakasNew]

no_slip_bc = [MMS, Colagrossi, Marrone, Adami, Hashemi, Randles, Takeda, Esmaili]
no_slip_bc_no_pack = [MMS, Colagrossi, Marrone, Adami, Hashemi, Randles, Esmaili]
slip_bc = [MMS, Colagrossi, Marrone, Adami]

inlet = [MMS, Mirror, Hybrid, MirrorNew]
outlet = [MMS, Donothing, Mirror, Hybrid, MirrorNew]

pressure_bc.reverse()
pressure_bc_2.reverse()
no_slip_bc.reverse()
no_slip_bc_no_pack.reverse()
slip_bc.reverse()
inlet.reverse()
outlet.reverse()

# -------------------------------------------------------------------------
# Comparison of the different schemes.
class MMSCompare(Comparison):
    def get_name(self):
        return 'mms_compare'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        if 'mms1' in params:
            return 'MMS1'
        elif 'mms2' in params:
            return 'MMS2'
        elif 'mms3' in params:
            return 'MMS3'
        elif 'mms4' in params:
            return 'MMS4'
        elif 'mms6' in params:
            return 'MMS6'
        elif 'mms7' in params:
            return 'MMS7'
        elif 'mms8' in params:
            return 'MMS8'

    def setup(self):
        cmd = ' --max-step 1 --perturb pack '
        self._make_case(comp1, tf=0.1, common_cmd=cmd)
        self._set_re('MMS1')


class DirichletBCCompare(Comparison):
    def get_name(self):
        return 'dirichlet_bc'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        print(params)
        if 'tsph' in params:
            return "MMS"
        elif 'tsph_dirc_u' in params:
            return "velocity BC"
        elif 'tsph_dirc_p' in params:
            return "pressure BC"


    def setup(self):
        cmd = ' --max-step 100 --perturb pack '
        self._make_case(dirch_bc, tf=0.1, common_cmd=cmd)
        self._set_re('MMS')


class PressureBCCompare(Comparison):
    def get_name(self):
        return 'pressure_bc_domain1'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        print(params)
        if 'mms' in params:
            if '--nl' in params:
                return "MMS 2L"
            else:
                return "MMS"
        elif 'marongiu' in params:
            return 'Marongiu'
        elif 'hashemi' in params:
            return "Hashemi"
        elif 'colagrossi' in params:
            return "Colagrossi"
        elif 'marrone' in params:
            return "Marrone"
        elif 'adami' in params:
            return "Adami"
        elif 'new' in params:
            return "New"
        elif 'new_marrone' in params:
            return "Rkpm Marrone"
        elif 'new_fourtakas' in params:
            return "Fourtakas"


    def setup(self):
        cmd = ' --max-step 200 --bc p_solid --mms mms_pres_d1 --nu=0.0 --timestep 0.000023 --domain 1 '
        self._make_case(pressure_bc, tf=0.1, common_cmd=cmd, name='_p_d1', err=ERRORS)
        self._set_re('MMS')


class PressureBCCompareDomain1(PressureBCCompare):
    def get_name(self):
        return 'pressure_bc_d1'

    def setup(self):
        cmd = ' --max-step %d %s --bc p_solid --mms mms_pres_d1 --nu=0.0 --timestep 0.000023 --domain 1 '%(STEPS, INTG)
        self._make_case(pressure_bc, tf=0.1, common_cmd=cmd, name='_p_d1', err=ERRORS)
        self._set_re('MMS')


class PressureBCCompareDomain5(PressureBCCompare):
    def get_name(self):
        return 'pressure_bc_d5'

    def setup(self):
        cmd = ' --max-step %d %s --bc p_solid --mms mms_pres_d5 --nu=0.0 --timestep 0.000023 --domain 5 '%(STEPS, INTG)
        self._make_case(pressure_bc_2, tf=0.1, common_cmd=cmd, name='_p_d5', err=ERRORS)
        self._set_re('MMS')


class PressureBCCompareDomain5Pack(PressureBCCompare):
    def get_name(self):
        return 'pressure_bc_d5_pack'

    def setup(self):
        cmd = ' --max-step %d %s --bc p_solid --mms mms_pres_d5 --nu=0.0 --timestep 0.000023 --domain 5 --pack 1 '%(STEPS, INTG)
        self._make_case(pressure_bc_2, tf=0.1, common_cmd=cmd, name='_p_d5_pack', err=ERRORS)
        self._set_re('MMS')


class PressureBCCompareDomain6(PressureBCCompare):
    def get_name(self):
        return 'pressure_bc_d6'

    def setup(self):
        # both d5 and d6 has same mms
        cmd = ' --max-step %d %s --bc p_solid --mms mms_pres_d5 --nu=0.0 --timestep 0.000023 --domain 6 '%(STEPS, INTG)
        self._make_case(pressure_bc_2, tf=0.1, common_cmd=cmd, name='_p_d6', err=ERRORS)
        self._set_re('MMS')


class PressureBCCompareDomain6Pack(PressureBCCompare):
    def get_name(self):
        return 'pressure_bc_d6_pack'

    def setup(self):
        # both d5 and d6 has same mms
        cmd = ' --max-step %d %s --bc p_solid --mms mms_pres_d5 --nu=0.0 --timestep 0.000023 --domain 6 --pack 1 '%(STEPS, INTG)
        self._make_case(pressure_bc_2, tf=0.1, common_cmd=cmd, name='_p_d6_pack', err=ERRORS)
        self._set_re('MMS')


class SlipBCCompare(Comparison):
    def get_name(self):
        return 'slip'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        print(params)
        if 'mms' in params:
            if '--nl' in params:
                return "MMS 2L"
            else:
                return "MMS"
        elif 'takeda' in params:
            return "Takeda"
        elif 'adami' in params:
            return "Adami"
        elif 'hashemi' in params:
            return "Hashemi"
        elif 'randles' in params:
            return "Randles"
        elif 'colagrossi' in params:
            return "Colagrossi"
        elif 'marrone' in params:
            return "Marrone"
        elif 'esmaili' in params:
            return "Esmaili"
        elif 'new' in params:
            return "New"
        elif 'new_marrone' in params:
            return "Rkpm Marrone"
        elif 'new_fourtakas' in params:
            return "Fourtakas"


class SlipBCCompareDomain1(SlipBCCompare):
    def get_name(self):
        return 'slip_d1'

    def setup(self):
        cmd = ' --max-step %d %s --bc u_slip --mms mms_slip_d1 --nu=0.0 --timestep 0.000023 --domain 1 '%(STEPS, INTG)
        self._make_case(slip_bc, tf=0.1, common_cmd=cmd, name='_us_d1', err=ERRORS)
        self._set_re('MMS')


class SlipBCCompareDomain5(SlipBCCompare):
    def get_name(self):
        return 'slip_d5'

    def setup(self):
        cmd = ' --max-step %d %s  --bc u_slip --mms mms_slip_d5 --nu=0.0 --timestep 0.000023 --domain 5 '%(STEPS, INTG)
        self._make_case(slip_bc, tf=0.1, common_cmd=cmd, name='_us_d5', err=ERRORS)
        self._set_re('MMS')


class SlipBCCompareDomain5pack(SlipBCCompare):
    def get_name(self):
        return 'slip_d5_pack'

    def setup(self):
        cmd = ' --max-step %d %s  --bc u_slip --mms mms_slip_d5 --nu=0.0 --timestep 0.000023 --domain 5 --pack 1 '%(STEPS, INTG)
        self._make_case(slip_bc, tf=0.1, common_cmd=cmd, name='_us_d5_pack', err=ERRORS)
        self._set_re('MMS')


class SlipBCCompareDomain6(SlipBCCompare):
    def get_name(self):
        return 'slip_d6'

    def setup(self):
        cmd = ' --max-step %d %s  --bc u_slip --mms mms_slip_d5 --nu=0.0 --timestep 0.000023 --domain 6 '%(STEPS, INTG)
        self._make_case(slip_bc, tf=0.1, common_cmd=cmd, name='_us_d6', err=ERRORS)
        self._set_re('MMS')


class SlipBCCompareDomain6pack(SlipBCCompare):
    def get_name(self):
        return 'slip_d6_pack'

    def setup(self):
        cmd = ' --max-step %d %s  --bc u_slip --mms mms_slip_d5 --nu=0.0 --timestep 0.000023 --domain 6 --pack 1 '%(STEPS, INTG)
        self._make_case(slip_bc, tf=0.1, common_cmd=cmd, name='_us_d6_pack', err=ERRORS)
        self._set_re('MMS')


class NoSlipBCCompare(Comparison):
    def get_name(self):
        return 'no_slip'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        print(params)
        if 'mms' in params:
            if '--nl' in params:
                return "MMS 2L"
            else:
                return "MMS"
        elif 'takeda' in params:
            return "Takeda"
        elif 'adami' in params:
            return "Adami"
        elif 'hashemi' in params:
            return "Hashemi"
        elif 'randles' in params:
            return "Randles"
        elif 'colagrossi' in params:
            return "Colagrossi"
        elif 'marrone' in params:
            return "Marrone"
        elif 'esmaili' in params:
            return "Esmaili"
        elif 'new' in params:
            return "New"
        elif 'new_marrone' in params:
            return "Rkpm Marrone"
        elif 'new_fourtakas' in params:
            return "Fourtakas"


    def setup(self):
        cmd = ' --max-step %d %s --bc u_no_slip --mms mms11 --nu=0.25 --timestep 0.000023 '%(STEPS, INTG)
        self._make_case(no_slip_bc, tf=0.1, common_cmd=cmd, name='_uns', err=ERRORS)
        self._set_re('MMS')


class NoSlipBCCompareDomain1(NoSlipBCCompare):
    def get_name(self):
        return 'no_slip_d1'

    def setup(self):
        cmd = ' --max-step %d %s --bc u_no_slip --mms mms_noslip_d1 --nu=0.25 --timestep 0.000023 --domain 1 '%(STEPS, INTG)
        self._make_case(no_slip_bc, tf=0.1, common_cmd=cmd, name='_uns_d1', err=ERRORS)
        self._set_re('MMS')


class NoSlipBCCompareDomain5(NoSlipBCCompare):
    def get_name(self):
        return 'no_slip_d5'

    def setup(self):
        cmd = ' --max-step %d %s --bc u_no_slip --mms mms_noslip_d5 --nu=0.25 --timestep 0.000023 --domain 5 '%(STEPS, INTG)
        self._make_case(no_slip_bc_no_pack, tf=0.1, common_cmd=cmd, name='_uns_d5', err=ERRORS)
        self._set_re('MMS')


class NoSlipBCCompareDomain5Pack(NoSlipBCCompare):
    def get_name(self):
        return 'no_slip_d5_pack'

    def setup(self):
        cmd = ' --max-step %d %s --bc u_no_slip --mms mms_noslip_d5 --nu=0.25 --timestep 0.000023 --domain 5 --pack 1 '%(STEPS, INTG)
        self._make_case(no_slip_bc, tf=0.1, common_cmd=cmd, name='_uns_d5_pack', err=ERRORS)
        self._set_re('MMS')


class NoSlipBCCompareDomain6(NoSlipBCCompare):
    def get_name(self):
        return 'no_slip_d6'

    def setup(self):
        cmd = ' --max-step %d %s --bc u_no_slip --mms mms_noslip_d6 --nu=0.25 --timestep 0.000023 --domain 6 '%(STEPS, INTG)
        self._make_case(no_slip_bc_no_pack, tf=0.1, common_cmd=cmd, name='_uns_d6', err=ERRORS)
        self._set_re('MMS')


class NoSlipBCCompareDomain6Pack(NoSlipBCCompare):
    def get_name(self):
        return 'no_slip_d6_pack'

    def setup(self):
        cmd = ' --max-step %d %s --bc u_no_slip --mms mms_noslip_d6 --nu=0.25 --timestep 0.000023 --domain 6 --pack 1 '%(STEPS, INTG)
        self._make_case(no_slip_bc, tf=0.1, common_cmd=cmd, name='_uns_d6_pack', err=ERRORS)
        self._set_re('MMS')



class IOCompare(Comparison):
    def get_name(self):
        return 'u_outlet'

    def get_label(self, problem):
        params = problem.cases[0].command.split()

        print(params)
        if 'mms' in params:
            if '--nl' in params:
                return "MMS 2L"
            else:
                return "MMS"
        elif 'donothing' in params:
            return "Do-nothing"
        elif 'mirror' in params:
            return "Mirror"
        elif 'hybrid' in params:
            return "Hybrid"
        elif 'mirror_new' in params:
            return "Simple Mirror"


class VelocityInlet(IOCompare):
    def get_name(self):
        return 'u_inlet'

    def setup(self):
        cmd = ' --max-step %d %s --bc u_inlet --mms mms_io_vel --nu=0.25 --timestep 0.000012 --pst-freq 0 '%(STEPS, INTG)
        self._make_case(inlet, tf=0.1, common_cmd=cmd, name='_u_in', filename='mms_main_io.py',
                        err=ERRORS)
        self._set_re('MMS')


class VelocityOutlet(IOCompare):
    def get_name(self):
        return 'u_outlet'

    def setup(self):
        cmd = ' --max-step %d %s --bc u_outlet --mms mms_io_vel --nu=0.25 --timestep 0.000012  --pst-freq 0 '%(STEPS, INTG)
        self._make_case(outlet, tf=0.1, common_cmd=cmd, name='_u_out', filename='mms_main_io.py',
                        err=ERRORS)
        self._set_re('MMS')


class PressureInlet(IOCompare):
    def get_name(self):
        return 'p_inlet'

    def setup(self):
        cmd = ' --max-step %d %s --bc p_inlet --mms mms_io_pres --nu=0.25 --timestep 0.000012  --pst-freq 0 '%(STEPS, INTG)
        self._make_case(inlet , tf=0.1, common_cmd=cmd, name='_p_in', filename='mms_main_io.py',
                        err=ERRORS)
        self._set_re('MMS')


class PressureOutlet(IOCompare):
    def get_name(self):
        return 'p_outlet'

    def setup(self):
        cmd = ' --max-step %d %s --bc p_outlet --mms mms_io_pres --nu=0.25 --timestep 0.000012  --pst-freq 0 '%(STEPS, INTG)
        self._make_case(outlet, tf=0.1, common_cmd=cmd, name='_p_out', filename='mms_main_io.py',
                        err=ERRORS)
        self._set_re('MMS')


class VelocityInletWave(IOCompare):
    def get_name(self):
        return 'u_inlet_wave'

    def setup(self):
        cmd = ' --max-step %d %s --bc u_inlet --mms mms_in_vel_wave --nu=0.0 --timestep 0.000012 --pst-freq 0 '%(500, INTG)
        self._make_case(inlet, tf=0.1, common_cmd=cmd, name='_u_in_wave', filename='mms_main_io.py',
                        err=ERRORS)
        self._set_re('MMS')


class VelocityOutletWave(IOCompare):
    def get_name(self):
        return 'u_outlet_wave'

    def setup(self):
        cmd = ' --max-step %d %s --bc u_outlet --mms mms_out_vel_wave --nu=0.0 --timestep 0.000012  --pst-freq 0 '%(500, INTG)
        self._make_case(outlet, tf=0.1, common_cmd=cmd, name='_u_out_wave', filename='mms_main_io.py',
                        err=ERRORS)
        self._set_re('MMS')


class PressureInletWave(IOCompare):
    def get_name(self):
        return 'p_inlet_wave'

    def setup(self):
        cmd = ' --max-step %d %s --bc p_inlet --mms mms_inlet_pres --nu=0.0 --timestep 0.000012  --pst-freq 0 '%(500, INTG)
        self._make_case(inlet , tf=0.1, common_cmd=cmd, name='_p_in_wave', filename='mms_main_io.py',
                        err=ERRORS)
        self._set_re('MMS')


class PressureOutletWave(IOCompare):
    def get_name(self):
        return 'p_outlet_wave'

    def setup(self):
        cmd = ' --max-step %d %s --bc p_outlet --mms mms_outlet_pres --nu=0.0 --timestep 0.000012  --pst-freq 0 '%(500, INTG)
        self._make_case(outlet, tf=0.1, common_cmd=cmd, name='_p_out_wave', filename='mms_main_io.py',
                        err=ERRORS)
        self._set_re('MMS')


def plot_domain(domain=1, pack=0, mms='mms'):
    import sys
    sys.path.append('code')
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import make_axes_locatable
    particles = get_particles(domain, pack, mms)

    fluid = particles[0]
    vmag = np.sqrt(fluid.u**2 + fluid.v**2)

    fig, axes = plt.subplots(1, 2, sharey=True, sharex=True, figsize=(12,5))
    axes = np.ravel(axes)

    img = axes[0].scatter(fluid.x, fluid.y, c=vmag)
    divider = make_axes_locatable(axes[0])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(img, cax=cax, orientation='vertical')
    axes[0].set_title("Velocity")
    axes[0].grid(False)

    img = axes[1].scatter(fluid.x, fluid.y, c=fluid.p)
    divider = make_axes_locatable(axes[1])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    fig.colorbar(img, cax=cax, orientation='vertical')
    axes[1].set_title("Pressure")
    axes[1].grid(False)

    filename = 'manuscript/figures/d%d_p%s_%s.png'%(domain, pack, mms)
    plt.savefig(filename, dpi=300)
    plt.close()

def get_particles(domain, pack, mms="mms"):
    import sys
    sys.path.append('code')
    from particle_arrays import create_particles
    from config_solid_bc import set_bc_props, get_bc_require
    from mms_main import MSMethod

    app = MSMethod()
    app.nl = 6
    app.dx = 0.02
    app.volume = app.dx**2
    app.hdx = 1.2
    app.c0 = 20
    app.mms = mms

    is_mirror, is_boundary, is_ghost, is_ghost_mirror, is_boundary_shift = get_bc_require(
        'mms')
    domain = domain
    particles = create_particles(app, 1.0, 1.0, is_mirror, is_boundary,
                                    is_ghost, is_ghost_mirror,
                                    is_boundary_shift, domain, pack)
    return particles

def get_io_particles():
    import sys
    sys.path.append('code')
    from particle_arrays import create_particles_io
    from mms_main import MSMethod
    import matplotlib.pyplot as plt
    s=5.0

    app = MSMethod()
    app.nl = 6
    app.dx = 0.02
    app.volume = app.dx**2
    app.hdx = 1.2
    app.c0 = 20
    app.mms = 'mms_pres_d1'

    mirror_inlet, mirror_outlet = False, False
    particles = create_particles_io(app, 1.0, 1.0, mirror_inlet, mirror_outlet)

    fluid = particles[0]
    wall = particles[1]
    inlet = particles[2]
    outlet = particles[3]

    plt.scatter(fluid.x, fluid.y, s= s)
    plt.scatter(wall.x,wall.y, s= s)
    plt.scatter(inlet.x,inlet.y, s= s)
    plt.scatter(outlet.x,outlet.y, s= s)

    filename = 'manuscript/figures/domain_io.png'
    plt.savefig(filename, dpi=300)
    plt.close()


def domains_no_pack():
    import sys
    sys.path.append('code')
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import make_axes_locatable

    fig, axes = plt.subplots(1, 3, sharey=True, sharex=True, figsize=(16,5))
    axes = np.ravel(axes)
    s=5.0

    particles = get_particles(1, 0, "mms_pres_d1")
    fluid = particles[0]
    solid0 = particles[1]
    solid1 = particles[2]
    img = axes[0].scatter(fluid.x, fluid.y,s=s)
    img = axes[0].scatter(solid1.x, solid1.y, s=s)
    img = axes[0].scatter(solid0.x, solid0.y, s=s)
    axes[0].set_title("Straight")

    particles = get_particles(5, 0, "mms_pres_d1")
    fluid = particles[0]
    solid0 = particles[1]
    solid1 = particles[2]
    img = axes[1].scatter(fluid.x, fluid.y,s=s)
    img = axes[1].scatter(solid1.x, solid1.y, s=s)
    img = axes[1].scatter(solid0.x, solid0.y, s=s)
    axes[1].set_title("Convex")

    particles = get_particles(6, 0, "mms_pres_d1")
    fluid = particles[0]
    solid0 = particles[1]
    solid1 = particles[2]
    img = axes[2].scatter(fluid.x, fluid.y,s=s)
    img = axes[2].scatter(solid1.x, solid1.y, s=s)
    img = axes[2].scatter(solid0.x, solid0.y, s=s)
    axes[2].set_title("Concave")

    filename = 'manuscript/figures/domain_0.png'
    plt.savefig(filename, dpi=300)
    plt.close()


def domains_pack():
    import sys
    sys.path.append('code')
    import matplotlib.pyplot as plt
    from mpl_toolkits.axes_grid1 import make_axes_locatable

    fig, axes = plt.subplots(1, 2, sharey=True, sharex=True, figsize=(10,5))
    axes = np.ravel(axes)
    s=5.0

    particles = get_particles(5, 1, "mms_pres_d1")
    fluid = particles[0]
    solid0 = particles[1]
    solid1 = particles[2]
    img = axes[0].scatter(fluid.x, fluid.y,s=s)
    img = axes[0].scatter(solid1.x, solid1.y, s=s)
    img = axes[0].scatter(solid0.x, solid0.y, s=s)
    axes[0].set_title("Packed-convex")

    particles = get_particles(6, 1, "mms_pres_d1")
    fluid = particles[0]
    solid0 = particles[1]
    solid1 = particles[2]
    img = axes[1].scatter(fluid.x, fluid.y,s=s)
    img = axes[1].scatter(solid1.x, solid1.y, s=s)
    img = axes[1].scatter(solid0.x, solid0.y, s=s)
    axes[1].set_title("Packed-concave")

    filename = 'manuscript/figures/domain_1.png'
    plt.savefig(filename, dpi=300)
    plt.close()


def create_solid_bc():
    plot_domain(1, 0, 'mms_pres_d1')
    plot_domain(5, 0, 'mms_pres_d5')

    plot_domain(1, 0, 'mms_noslip_d1')
    plot_domain(5, 0, 'mms_noslip_d5')
    plot_domain(6, 0, 'mms_noslip_d6')

    plot_domain(1, 0, 'mms_slip_d1')
    plot_domain(5, 0, 'mms_slip_d5')

    plot_domain(1, 0, 'mms_io_vel')
    plot_domain(1, 0, 'mms_in_vel_wave')
    plot_domain(1, 0, 'mms_out_vel_wave')

    plot_domain(1, 0, 'mms_io_pres')
    plot_domain(1, 0, 'mms_inlet_pres')
    plot_domain(1, 0, 'mms_outlet_pres')


class CompareSlipVelocities(PySPHProblem):
    def get_name(self):
        return "slip_vel"

    def setup(self):
        get_path = self.input_path
        self.nx = RESOLUTIONS
        n_core = NCORE
        n_thread = NTHREAD
        self.bctypes = ['adami', 'marrone', 'colagrossi', 'new']
        self.domain = [1, 3, 4, 5]

        _cmd = ' python code/function_approx.py --openmp --bc u_slip'
        cases = [
            Simulation(
                get_path('%s_%d_%d' % (method, nx, domain)), _cmd,
                job_info=dict(
                    n_core=n_core, n_thread=n_thread), nx=nx, bctype=method, domain=domain,
                    mms="mms3" if (domain==1) else "mms5" if (domain==3) else "mmsc2" if (domain == 5) else "mms8"
            )
            for method in self.bctypes for nx in self.nx for domain in self.domain
        ]

        self.cases = cases

    def run(self):
        self.make_output_dir()
        for domain in self.domain:
            self.plot_velocities(domain)
            self.plot_convergence(domain)

    def plot_velocities(self, domain):
        import matplotlib.pyplot as plt
        cases = filter_cases(self.cases, nx=100, domain=domain)
        ycheck, uac = None, None
        x0, x1, y = None, None, None
        for case in cases:
            data = case.data
            ycheck = data['ycheck']
            uac = data['uac']
            u = data['u']
            x0 = data['x0']
            x1 = data['x1']
            y = data['y']
            method = case.params['bctype']

            plt.plot(ycheck, u, label=method)

        plt.plot(ycheck, uac, '--k', label='expected')
        plt.plot(x0, y, '-r')
        plt.plot(x1, y, '--r')
        plt.grid()
        plt.legend()

        plt.savefig(self.output_path(self.get_name() + '_%d_vel.png'%domain))
        plt.close()

    def plot_convergence(self, domain):
        import matplotlib.pyplot as plt
        cases = filter_cases(self.cases, domain=domain)
        errs = None
        nxs = None
        for bctype in self.bctypes:
            _cases = filter_cases(cases, bctype=bctype)
            nxs = []
            errs = []
            for nx in self.nx:
                __cases = filter_cases(_cases, nx=nx)
                data = __cases[0].data
                nxs.append(nx)
                errs.append(data['err'])
            plt.loglog(nxs, errs, label=bctype)

            nxs = np.array(nxs)
            plt.loglog(nxs, errs[0] * (nxs[0]/nxs)**2, '--k')
            plt.loglog(nxs, errs[0] * (nxs[0]/nxs), '-k')
            print(errs, bctype)

        plt.grid()
        plt.legend()
        plt.ylabel(r"$L_1$")
        plt.xlabel("N")

        plt.savefig(self.output_path(self.get_name() + '_%d_conv.png'%domain))
        plt.close()


class CompareNoSlipVelocities(CompareSlipVelocities):
    def get_name(self):
        return "no_slip_vel"

    def setup(self):
        get_path = self.input_path
        self.nx = RESOLUTIONS
        n_core = NCORE
        n_thread = NTHREAD
        self.bctypes = ['adami', 'marrone', 'colagrossi', 'takeda', 'esmaili', 'new']
        self.bctypes = ['marrone', 'new_marrone']
        self.domain = [1, 3, 4]
        self.domain = [1]

        _cmd = ' python code/function_approx.py --openmp --bc u_no_slip'
        cases = [
            Simulation(
                get_path('%s_%d_%d' % (method, nx, domain)), _cmd,
                job_info=dict(
                    n_core=n_core, n_thread=n_thread), nx=nx, bctype=method, domain=domain,
                    mms="mms7" if (domain==1) else "mms6" if (domain==3) else "mms9"
            )
            for method in self.bctypes for nx in self.nx for domain in self.domain
        ]

        self.cases = cases


class ComparePressureBC(CompareSlipVelocities):
    def get_name(self):
        return "pres_bc"

    def setup(self):
        get_path = self.input_path
        self.nx = RESOLUTIONS
        n_core = NCORE
        n_thread = NTHREAD
        # self.bctypes = ['adami', 'marrone', 'marongiu', 'colagrossi']
        self.bctypes = ['adami', 'marrone', 'marongiu', 'colagrossi', 'new', 'new_marrone']
        self.domain = [1, 3, 4, 5]
        self.domain = [1]

        _cmd = ' python code/function_approx.py --openmp --bc p_solid '
        cases = [
            Simulation(
                get_path('%s_%d_%d' % (method, nx, domain)), _cmd,
                job_info=dict(
                    n_core=n_core, n_thread=n_thread), nx=nx, bctype=method, domain=domain,
                    mms="mms4" if (domain==1) else "mms2" if (domain==3) else "mmsc1" if (domain == 5) else "mms1"
            )
            for method in self.bctypes for nx in self.nx for domain in self.domain
        ]

        self.cases = cases

    def plot_velocities(self, domain):
        import matplotlib.pyplot as plt
        cases = filter_cases(self.cases, nx=100, domain=domain)
        ycheck, uac = None, None
        x0, x1, y = None, None, None
        for case in cases:
            data = case.data
            ycheck = data['ycheck']
            uac = data['pac']
            u = data['p']
            x0 = data['x0']
            x1 = data['x1']
            y = data['y']
            method = case.params['bctype']

            plt.plot(ycheck, u, label=method)

        plt.plot(ycheck, uac, '--k', label='expected')
        plt.plot(x0, y, '-r')
        plt.plot(x1, y, '--r')
        plt.grid()
        plt.legend()

        plt.savefig(self.output_path(self.get_name() + '_%d_pres.png'%domain))
        plt.close()


class FiniteDomainApproximation(PySPHProblem):
    def get_name(self):
        return "finite_domain"

    def setup(self):
        get_path = self.input_path
        self.nx = RESOLUTIONS
        n_core = NCORE
        n_thread = NTHREAD
        self.approx = ['first', 'second']

        _cmd = ' python code/finite_domain.py --openmp '
        cases = [
            Simulation(
                get_path('%s_%d' % (approx, nx)), _cmd,
                job_info=dict(
                    n_core=n_core, n_thread=n_thread), nx=nx, approx=approx
            )
            for approx in self.approx for nx in self.nx
        ]

        self.cases = cases

    def run(self):
        self.make_output_dir()
        self.plot_convergence('first')
        self.plot_convergence('second')

    def plot_convergence(self, approx='first'):
        import matplotlib.pyplot as plt
        cases = filter_cases(self.cases, approx=approx)
        layers = [0, 1, 2, 3, 4]

        nxs = None
        errs = None

        for layer in layers:
            nxs = []
            errs = []
            for case in cases:
                data = case.data
                exact = data['exact']
                comp = data['comp']
                x = data['x']
                y = data['y']
                nx = case.params['nx']
                diff = 1/nx * layer
                lim = 1.0 - diff
                cond = (x > diff) & (x <lim) & (y > diff) & (y<lim)
                err = sum(abs((exact - comp)[cond]))/len(exact[cond])
                # err = max(abs(comp - exact)[cond])
                errs.append(err)
                nxs.append(nx)
            plt.loglog(nxs, errs, '--o', label='%d layers'%(layer))

        nxs = np.array(nxs)
        plt.loglog(nxs, errs[0] * (nxs[0]/nxs)**2, '--k')
        plt.loglog(nxs, errs[0] * (nxs[0]/nxs), ':k')
        plt.ylabel(r"$L_{1}$")
        plt.xlabel("N")
        plt.grid()
        plt.legend()

        plt.savefig(self.output_path(self.get_name() + '_%s.png'%approx))
        plt.close()


class PerformanceComparison(PySPHProblem):
    def get_name(self):
        return "performance"

    def setup(self):
        get_path = self.input_path
        self.nx = RESOLUTIONS
        n_core = 10
        self.n_thread = [1, 2, 4, 8]
        self.method = ['marrone', 'colagrossi', 'adami']

        _cmd = ' python code/mms_main.py --bc u_no_slip --domain 1'\
            ' --mms mms_noslip_d1 --nx 100 --openmp '\
            ' --nu 0.25 --max-step 100 --timestep 0.000012 --pf 10 '
        cases = [ Simulation(
                get_path('%s_%d' % (approx, thread)), _cmd,
                job_info=dict(
                    n_core=n_core, n_thread=thread), bctype=approx
            )
            for approx in self.method for thread in self.n_thread
        ]

        self.cases = cases

    def run(self):
        self.make_output_dir()
        self._plot_time_vs_thread()

    def _plot_time_vs_thread(self):
        import matplotlib.pyplot as plt

        for method in self.method:
            times = []
            for th in self.n_thread:

                cases = filter_cases(self.cases, bctype=method)
                _case = None
                for case in cases:
                    if case.job_info['n_thread'] == th:
                        _case = case
                        break
                time = get_cpu_time(_case)
                times.append(time)

            plt.plot(self.n_thread, times, label=method.title())

        plt.ylabel("time (sec)")
        plt.xlabel("No. of threads")
        plt.grid('on')
        plt.legend()

        plt.savefig(self.output_path(self.get_name() + '_perf.png'))
        plt.close()


class SinglePulse(PySPHProblem):
    def get_name(self):
        return 'pulse'

    def get_re(self):
        return 0.0

    def _get_cmd(self):
        return 'python code/pulse.py --tf 4 --pfreq 4 --openmp --bc u_outlet --nu 0.0 '


    def setup(self):
        self.umax = 1.0
        self.rho = 1000
        self.lt = 2.0
        self.nx = 40
        self.wt = 2.0
        self.data = {}
        re = self.get_re()
        ios = [
           'donothing', 'mirror', 'hybrid', 'mirror_new'
        ]

        get_path = self.input_path
        cmd = self._get_cmd()

        self.cases = [
            Simulation(
                get_path(method), cmd, job_info=dict(n_core=NCORE, n_thread=NTHREAD),
                cache_nnps=None, bctype=method, lt=self.lt, nx=self.nx, max_step= 225 if method == 'mirror' else 875 if method == 'mirror_new' else 1000
            )
            for method in ios
        ]

        cmd_exact = self._get_cmd()

        self.cases.append(
            Simulation(
                get_path('exact'), cmd_exact,
                job_info=dict(n_core=NCORE, n_thread=NTHREAD),
                cache_nnps=None, bctype='donothing', lt=40, nx=self.nx,
                max_step=1000
            )
        )

    def remove_data(self):
        if CLEAN:
            import glob
            import os
            for case in self.cases:
                dirname = case.input_path()
                path = os.path.join(dirname , 'pulse*.hdf5')
                names0 = glob.glob(path)
                path = os.path.join(dirname , 'pulse*.npz')
                names1 = glob.glob(path)
                names = names0 + names1
                for name in names:
                    print("removing", name)
                    os.remove(name)


    def run(self):
        self.make_output_dir()
        self.plot_pressure('p')
        self.plot_pressure('v')
        self.remove_data()

    def plot_pressure(self, var='p'):
        import matplotlib.pyplot as plt
        fig = plt.figure()
        for case in self.cases:
            method = case.params['bctype']
            lt = case.params['lt']
            if (lt > 5):
                method = 'exact'
            method = method.replace('_', ' ')
            method = method.title()
            method = method.replace(' ', '')
            print(method)
            data = case.data
            t = data['t']
            _var = data[var]
            plt.plot(t, _var, COLORS_SOLID[method], label=method)


        plt.ylabel('Pressure' if var=='p' else 'Velocity')
        plt.xlabel("time (sec)")
        plt.grid()
        plt.legend()

        plt.savefig(self.output_path(self.get_name() + '_t_vs_%s.png'%var))
        plt.close()


if __name__ == '__main__':
    from code.mms_creator import create_mms

    create_mms()
    # create_solid_bc()
    domains_no_pack()
    domains_pack()
    # get_io_particles()

    PROBLEMS = [
        PressureBCCompareDomain1,
        PressureBCCompareDomain5,
        PressureBCCompareDomain5Pack,
        PressureBCCompareDomain6,
        PressureBCCompareDomain6Pack,
        VelocityInlet,
        VelocityOutlet,
        PressureInlet, PressureOutlet,
        SlipBCCompareDomain1,
        SlipBCCompareDomain5,
        SlipBCCompareDomain5pack,
        SlipBCCompareDomain6,
        SlipBCCompareDomain6pack,
        NoSlipBCCompareDomain1,
        NoSlipBCCompareDomain5,
        NoSlipBCCompareDomain5Pack,
        NoSlipBCCompareDomain6,
        NoSlipBCCompareDomain6Pack,
        FiniteDomainApproximation,
        VelocityInletWave, VelocityOutletWave,
        PressureInletWave, PressureOutletWave,
        PerformanceComparison
    ]

    automator = Automator(
        simulation_dir='outputs',
        output_dir=os.path.join('manuscript', 'figures'),
        all_problems=PROBLEMS
    )
    automator.run()
